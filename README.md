This repository contains the analysis plan for the IMEFA study.
IMEFA: Interoceptive and Metacognitive Factors of Fatigue in Multiple Sclerosis (IMEFA)

Lead of Analysis: Marion Rouault
Project Lead: Zina-Mary Manjaly
Project Members: Klaas Enno Stephan, Ines Pereira, Stephen Fleming, Fabien Vinckier
